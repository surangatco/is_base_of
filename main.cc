#include <iostream>


using namespace std;

class A
{
	public:
		virtual void show() {cout << "class A" << endl;}
};

class B :   public A
{
	public:
		virtual void show() {cout << "class B" << endl;}
};

class C
{
	public:
		virtual void show() {cout << "class C" << endl;}
};

template<class B>
bool helper(B*){return true;}

template<class B>
bool helper(...){return false;}

template<class D, class B>
bool is_base_ofc()
{
	return helper<B>(new D());
};


int main()
{
	A a;
	B b;
	C c;

	// cout << is_base_ofc<B, A>(b,a) << endl;
	// cout << is_base_ofc<C, A>(c,a) << endl;

	cout << is_base_ofc<B, A>() << endl;
	cout << is_base_ofc<C, A>() << endl;
	return 0;
}